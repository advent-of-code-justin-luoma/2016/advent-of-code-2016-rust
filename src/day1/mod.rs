use regex::Regex;

enum Direction {
    North,
    East,
    South,
    West,
}

fn parse(str: String) -> Vec<(String, u32)> {
    let re: Regex = Regex::new(r"(\w)(\d+)").unwrap();

    str.split(", ")
        .map(|s| {
            let caps = re.captures(s).unwrap();

            let dir = caps.get(1).unwrap().as_str();
            let dist: u32 = caps.get(2).unwrap().as_str().parse().unwrap();

            (dir.to_owned(), dist)
        })
        .collect()
}

fn update_heading(dir: &mut Direction, turn: &str) {
    match dir {
        Direction::North => match turn {
            "R" => *dir = Direction::East,
            "L" => *dir = Direction::West,
            _ => {}
        },
        Direction::East => match turn {
            "R" => *dir = Direction::South,
            "L" => *dir = Direction::North,
            _ => {}
        },
        Direction::South => match turn {
            "R" => *dir = Direction::West,
            "L" => *dir = Direction::East,
            _ => {}
        },
        Direction::West => match turn {
            "R" => *dir = Direction::North,
            "L" => *dir = Direction::South,
            _ => {}
        },
    }
}

pub fn day1_part2() {
    let parsed = parse(crate::read_file("./inputs/day1"));

    let mut visited = vec![];

    let (mut x, mut y) = (0, 0);

    let mut direction = Direction::North;

    let mut brk = false;

    for (dir, dist) in parsed.iter() {
        update_heading(&mut direction, dir);

        for _ in 1..=*dist {
            if !visited.contains(&(x, y)) {
                visited.push((x, y));
                match direction {
                    Direction::North => {
                        y += 1;
                    }
                    Direction::East => {
                        x += 1;
                    }
                    Direction::South => {
                        y -= 1;
                    }
                    Direction::West => {
                        x -= 1;
                    }
                }
            } else {
                println!("({x}, {y})");
                brk = true;
                break;
            }
        }
        if brk {
            break;
        }
    }
}
