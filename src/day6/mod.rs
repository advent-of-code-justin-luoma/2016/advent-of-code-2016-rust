use crate::read_lines;
use std::collections::HashMap;

pub fn part1() {
    let map: HashMap<usize, HashMap<char, usize>> = HashMap::new();
    let counts = read_lines("./inputs/day6").fold(
        map,
        |mut acc: HashMap<usize, HashMap<char, usize>>, line| {
            let line = line.unwrap();
            for (i, c) in line.chars().enumerate() {
                let entry = acc.entry(i).or_insert(HashMap::new());
                *entry.entry(c).or_insert(0) += 1;
            }
            acc
        },
    );

    let mut i = 0;
    while counts.contains_key(&i) {
        let max = counts
            .get(&i)
            .unwrap()
            .iter()
            .max_by(|(_, c1), (_, c2)| c1.cmp(c2))
            .unwrap();
        println!("{:?}", max);
        i += 1;
    }
}

pub fn part2() {
    let map: HashMap<usize, HashMap<char, usize>> = HashMap::new();
    let counts = read_lines("./inputs/day6").fold(
        map,
        |mut acc: HashMap<usize, HashMap<char, usize>>, line| {
            let line = line.unwrap();
            for (i, c) in line.chars().enumerate() {
                let entry = acc.entry(i).or_insert(HashMap::new());
                *entry.entry(c).or_insert(0) += 1;
            }
            acc
        },
    );

    let mut i = 0;
    while counts.contains_key(&i) {
        let max = counts
            .get(&i)
            .unwrap()
            .iter()
            .min_by(|(_, c1), (_, c2)| c1.cmp(c2))
            .unwrap();
        println!("{:?}", max);
        i += 1;
    }
}
