const LITHIUM: u8 = 1;
const HYDROGEN: u8 = 2;

pub fn example() {
    let initial = 
}































#[derive(Debug, Clone)]
struct Floor {
    chips: Vec<String>,
    generators: Vec<String>,
    elevator: bool,
}

impl PartialEq for Floor {
    fn eq(&self, other: &Self) -> bool {
        self.chips == other.chips && self.generators == other.generators // && self.elevator == other.elevator
    }
}

impl Eq for Floor {}

impl Default for Floor {
    fn default() -> Self {
        Floor::new(Default::default())
    }
}

impl Floor {
    fn new(elevator: bool) -> Self {
        Self {
            chips: Vec::new(),
            generators: Vec::new(),
            elevator,
        }
    }

    fn chips(&mut self) -> &mut Vec<String> {
        &mut self.chips
    }

    fn generators(&mut self) -> &mut Vec<String> {
        &mut self.generators
    }

    fn elevator(&mut self) -> &bool {
        &mut self.elevator
    }

    fn is_safe(&self) -> bool {
        let mut chips = self.chips.clone();
        let mut generators = self.generators.clone();
        chips.sort();
        generators.sort();

        chips == generators
    }
}
