pub mod solve;

use std::collections::hash_map::Entry::Occupied;
use std::collections::{HashMap, VecDeque};

use anyhow::{anyhow, Result};
use lazy_static::lazy_static;
use regex::Regex;

use crate::read_lines;

pub fn part1() {
    let mut ins: Vec<Ins> = read_lines("./inputs/day10")
        .map(|line| line.unwrap())
        .map(|text| parse(&text))
        .collect();

    let mut outputs: HashMap<usize, u32> = HashMap::new();
    let mut bots: HashMap<usize, Bot> = HashMap::new();

    let mut queue: VecDeque<Ins> = VecDeque::new();

    loop {
        if !queue.is_empty() {
            let tmp_queue = queue.clone();
            queue.clear();

            let iter = tmp_queue.iter();
            println!("{:?}", bots);
            process_instructions(iter, &mut bots, &mut outputs, &mut queue);
        }

        let iter = ins.iter();

        process_instructions(iter, &mut bots, &mut outputs, &mut queue);

        ins.clear();

        if ins.is_empty() && queue.is_empty() {
            break;
        }
        // match ins.peek() {
        //     Some(Ins::Value(_, _)) => process_value(&mut bots, &mut queue, ins.next().unwrap()),
        //     Some(_) => {
        //         let ins = ins.next().unwrap();
        //         queue.push_back(*ins);
        //     }
        //     // Some(Ins::Bot(_, _, _)) => {
        //     //     process_bot(&mut bots, &mut outputs, &mut queue, ins.next().unwrap())
        //     // }
        //     None => break,
        // }
    }

    // while !queue.is_empty() {
    //     match queue.pop_front() {
    //         Some(ins) => match ins {
    //             Ins::Value(_, _) => process_value(&mut bots, &mut queue, &ins),
    //             Ins::Bot(_, _, _) => process_bot(&mut bots, &mut outputs, &mut queue, &ins),
    //         },
    //         None => {}
    //     }
    // }

    println!("{:?}", bots);
    println!("{:?}", outputs);
}

fn process_instructions<'a, T> (
    iter: T,
    bots: &mut HashMap<usize, Bot>,
    outputs: &mut HashMap<usize, u32>,
    queue: &mut VecDeque<Ins>,
) where T: Iterator<Item = &'a Ins> {
    for ins in iter {
        match ins {
            Ins::Value(bot, chip) => {
                bots.entry(*bot)
                    .and_modify(|bot| {
                        bot.give_chip(*chip)
                            .or_else(|_| -> Result<()> {
                                queue.push_back(*ins);
                                Ok(())
                            })
                            .unwrap();
                    })
                    .or_insert(Bot {
                        chips: (Some(*chip), None),
                        compares: vec![],
                    });
            }
            Ins::Bot(id, command) => {
                if bots.contains_key(id) && can_run_command(&command, bots) {
                    let mut bot = bots.get(id).unwrap().to_owned();

                    if let Ok((chip1, chip2)) = bot.run_command(command) {
                        match chip1 {
                            Chip::Output(bin, value) => {
                                outputs
                                    .entry(bin)
                                    .and_modify(|v| *v = value)
                                    .or_insert(value);
                            }
                            Chip::Bot(bot, value) => {
                                bots.entry(bot)
                                    .and_modify(|bot| bot.give_chip(value).unwrap())
                                    .or_insert(Bot {
                                        chips: (Some(value), None),
                                        compares: vec![],
                                    });
                            }
                        }

                        match chip2 {
                            Chip::Output(bin, value) => {
                                outputs
                                    .entry(bin)
                                    .and_modify(|v| *v = value)
                                    .or_insert(value);
                            }
                            Chip::Bot(bot, value) => {
                                bots.entry(bot)
                                    .and_modify(|bot| bot.give_chip(value).unwrap())
                                    .or_insert(Bot {
                                        chips: (Some(value), None),
                                        compares: vec![],
                                    });
                            }
                        }

                        bots.insert(*id, bot);
                    } else {
                        queue.push_back(*ins);
                    }
                }
            }
        }
    }
}

fn can_run_command(command: &Command, bots: &mut HashMap<usize, Bot>) -> bool {
    let can_low = if let Dest::Bot(bot) = command.low {
        if let Occupied(bot) = bots.entry(bot) {
            !bot.get().is_full()
        } else {
            true
        }
    } else {
        true
    };

    let can_high = if let Dest::Bot(bot) = command.high {
        if let Occupied(bot) = bots.entry(bot) {
            !bot.get().is_full()
        } else {
            true
        }
    } else {
        true
    };

    can_low && can_high
}

// fn process_bot(
//     bots: &mut HashMap<usize, Bot>,
//     outputs: &mut HashMap<usize, u32>,
//     queue: &mut VecDeque<Ins>,
//     instruction: &Ins,
// ) {
//     if let Ins::Bot(bot, low_chip, high_chip) = instruction {
//         let mut updated = false;
//         if let Occupied(entry) = bots.entry(*bot) {
//             let bot = entry.get();
//             if let (Some(v1), Some(v2)) = bot.chips {
//                 if v1 > v2 {
//                     process_chip(low_chip, bots, v2, v1, outputs, queue, instruction);
//                     process_chip(high_chip, bots, v2, v1, outputs, queue, instruction);
//                 } else {
//                     process_chip(low_chip, bots, v1, v2, outputs, queue, instruction);
//                     process_chip(high_chip, bots, v1, v2, outputs, queue, instruction);
//                 }
//                 updated = true;
//             } else {
//                 queue.push_back(*instruction);
//             }
//         } else {
//             queue.push_back(*instruction);
//         }
//         if updated {
//             update_bot(bots, bot);
//         }
//     }
// }

// fn update_bot(bots: &mut HashMap<usize, Bot>, bot: &usize) {
//     bots.entry(*bot).and_modify(|bot| {
//         let chips = bot.chips;
//         bot.compares.push((chips.0.unwrap(), chips.1.unwrap()));
//         bot.chips = (None, None);
//     });
// }

// fn process_value(bots: &mut HashMap<usize, Bot>, queue: &mut VecDeque<Ins>, instruction: &Ins) {
//     if let Ins::Value(bot, value) = *instruction {
//         give_chip_to_bot(bots, queue, bot, value, instruction);
//     }
// }

// fn process_chip(
//     chip: &Chip,
//     bots: &mut HashMap<usize, Bot>,
//     low: u32,
//     high: u32,
//     outputs: &mut HashMap<usize, u32>,
//     queue: &mut VecDeque<Ins>,
//     instruction: &Ins,
// ) {
//     match chip {
//         Chip::Low(dest) => match dest {
//             Dest::Bot(dest) => {
//                 give_chip_to_bot(bots, queue, *dest, low, instruction);
//             }
//             Dest::Output(output) => {
//                 outputs.insert(*output, low);
//             }
//         },
//         Chip::High(dest) => match dest {
//             Dest::Bot(dest) => {
//                 give_chip_to_bot(bots, queue, *dest, high, instruction);
//             }
//             Dest::Output(output) => {
//                 outputs.insert(*output, high);
//             }
//         },
//     }
// }

// fn give_chip_to_bot(
//     bots: &mut HashMap<usize, Bot>,
//     queue: &mut VecDeque<Ins>,
//     bot: usize,
//     value: u32,
//     instruction: &Ins,
// ) {
//     bots.entry(bot)
//         .and_modify(|bot| match bot.chips {
//             (None, None) => {
//                 bot.chips = (Some(value), None);
//             }
//             (None, Some(v)) => {
//                 bot.chips = (Some(value), Some(v));
//             }
//             (Some(v), None) => {
//                 bot.chips = (Some(v), Some(value));
//             }
//             (Some(_), Some(_)) => queue.push_back(*instruction),
//         })
//         .or_insert(Bot {
//             chips: (Some(value), None),
//             compares: vec![],
//         });
// }

#[derive(Debug, Clone)]
struct Bot {
    chips: (Option<u32>, Option<u32>),
    compares: Vec<(u32, u32)>,
}

impl Bot {
    pub fn give_chip(&mut self, value: u32) -> Result<()> {
        match self.chips {
            (Some(_), Some(_)) => return Err(anyhow!("chips full")),
            (None, None) => {
                self.chips = (Some(value), None);
            }
            (None, Some(v2)) => {
                self.chips = (Some(value), Some(v2));
                self.compares(value, v2);
            }
            (Some(v1), None) => {
                self.chips = (Some(v1), Some(value));
                self.compares(v1, value);
            }
        }
        Ok(())
    }

    pub fn is_full(&self) -> bool {
        match self.chips {
            (Some(_), Some(_)) => true,
            _ => false,
        }
    }

    fn compares(&mut self, value1: u32, value2: u32) {
        self.compares.push((value1, value2));
    }

    pub fn run_command(&mut self, command: &Command) -> Result<(Chip, Chip)> {
        let chip1 = self.chips.0.ok_or_else(|| anyhow!("not enough chips"))?;
        let chip2 = self.chips.1.ok_or_else(|| anyhow!("not enough chips"))?;
        self.chips = (None, None);
        return if chip1 < chip2 {
            Ok((
                self.pass_chip(chip1, &command.low),
                self.pass_chip(chip2, &command.high),
            ))
        } else {
            Ok((
                self.pass_chip(chip2, &command.low),
                self.pass_chip(chip1, &command.high),
            ))
        };
    }

    fn pass_chip(&mut self, chip: u32, destination: &Dest) -> Chip {
        match destination {
            Dest::Output(bin) => Chip::Output(*bin, chip),
            Dest::Bot(bot) => Chip::Bot(*bot, chip),
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct Command {
    low: Dest,
    high: Dest,
}

#[derive(Debug, Clone, Copy)]
enum Chip {
    Output(usize, u32),
    Bot(usize, u32),
}

#[derive(Debug, Clone, Copy)]
enum Dest {
    Output(usize),
    Bot(usize),
}

#[derive(Debug, Clone, Copy)]
enum Ins {
    Value(usize, u32),
    Bot(usize, Command),
}

fn parse(instruction: &str) -> Ins {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(?:value\s(?P<chip>\d+).+(?P<bot>\d+))|(?:bot\s(?P<giver>\d+)\sgives\s(?P<dir1>\w+)\sto\s(?P<dest1>\w+)\s(?P<n1>\d+)\sand\s(?P<dir2>\w+)\sto\s(?P<dest2>\w+)\s(?P<n2>\d+))").unwrap();
    }

    let caps = RE.captures(instruction).unwrap();

    // println!("{}: {:?}", caps.len(), caps);

    return if let Some(chip) = caps.name("chip") {
        let bot = caps.name("bot").unwrap();
        Ins::Value(
            bot.as_str().parse().unwrap(),
            chip.as_str().parse().unwrap(),
        )
    } else {
        let giver = caps.name("giver").unwrap().as_str().parse().unwrap();
        let dest1 = caps.name("dest1").unwrap().as_str();
        let n1 = caps.name("n1").unwrap().as_str().parse().unwrap();
        let dest2 = caps.name("dest2").unwrap().as_str();
        let n2 = caps.name("n2").unwrap().as_str().parse().unwrap();

        let low = extract_destination(dest1, n1);
        let high = extract_destination(dest2, n2);

        let command = Command { low, high };

        Ins::Bot(giver, command)
    };
}

fn extract_destination(destination: &str, id: usize) -> Dest {
    match destination {
        "bot" => Dest::Bot(id),
        "output" => Dest::Output(id),
        _ => unreachable!(),
    }
}
