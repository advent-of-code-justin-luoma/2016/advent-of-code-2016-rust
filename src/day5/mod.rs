use md5::{Digest, Md5};

pub fn part1() {
    println!("{}", generate_password_v1("ugkcyxxp"));
}

pub fn part2() {
    println!("{}", generate_password_v2("ugkcyxxp"));
}

fn generate_password_v1(id: &str) -> String {
    let mut password = String::new();

    for i in 0.. {
        let test = format!("{id}{i}");
        let hash = md5sum(&test);
        if hash.starts_with("00000") {
            password.push_str(hash.get(5..6).unwrap());
            if password.len() == 8 {
                break;
            }
        }
    }

    password
}

fn generate_password_v2(id: &str) -> String {
    let fill = "~".to_string();
    let mut password: Vec<String> = vec![fill; 8];
    let mut filled = vec![];

    for i in 0.. {
        let test = format!("{id}{i}");
        let hash = md5sum(&test);
        if hash.starts_with("00000") {
            if let Ok(pos) = hash.get(5..6).unwrap().parse::<usize>() {
                if pos > 7 {
                    continue;
                }
                let letter = hash.get(6..7).unwrap();
                if !filled.contains(&pos) {
                    password[pos] = letter.to_string();
                    filled.push(pos);
                }
                if filled.len() == 8 {
                    break;
                }
            }
        }
    }

    password.join("")
}

fn md5sum(text: &str) -> String {
    let mut hasher = Md5::new();
    hasher.update(text.as_bytes());

    let hash = hasher.finalize();

    base16ct::lower::encode_string(&hash)
}
