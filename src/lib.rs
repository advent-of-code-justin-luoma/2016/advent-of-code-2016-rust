#![allow(dead_code)]
#![feature(slice_take)]

#[macro_use]
extern crate nom;

use std::fs::File;
use std::io::{BufRead, BufReader, Lines};
use std::{fs, io};

mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod day10;
mod day11;

pub fn run() {
    day10::solve::main();
}

fn read_file(file: &str) -> String {
    fs::read_to_string(file).expect("Couldn't read file")
}

fn read_lines(file: &str) -> Lines<BufReader<File>> {
    let file = File::open(file).expect("Couldn't open file");

    io::BufReader::new(file).lines()
}
