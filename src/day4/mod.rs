use std::collections::HashMap;
// use bitvec::prelude as bv;
use crate::read_lines;
use lazy_static::lazy_static;
use regex::{Captures, Regex};

fn discombobulate(text: &str) -> (String, String, String) {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"([a-z-]*-?)-(\d+)\[([a-z]+)\]").unwrap();
    }
    let caps: Captures = RE.captures(text).unwrap();
    (
        caps.get(1).unwrap().as_str().to_string(),
        caps.get(2).unwrap().as_str().to_string(),
        caps.get(3).unwrap().as_str().to_string(),
    )
}

pub fn part1() {
    let mut sum = 0;

    for line in read_lines("./inputs/day4") {
        let line = line.unwrap();
        if verify_checksum(line.trim()) {
            let (_, id, _) = discombobulate(&line);
            sum += id.parse::<u32>().unwrap();
        }
    }

    println!("{}", sum);
}

pub fn part2() {
    for line in read_lines("./inputs/day4") {
        let line = line.unwrap();
        if verify_checksum(line.trim()) {
            let (name, id, _) = discombobulate(&line);
            println!("{id}: {:?}", decrypt_name(&name, id.parse().unwrap()));
        }
    }
}

fn decrypt_name(text: &str, rot: u32) -> String {
    text.chars()
        .map(|c| match c {
            '-' => ' ',
            _ => wrapping_add(&c, rot),
        })
        .map(|c| c.to_string())
        .collect::<Vec<String>>()
        .join("")
}

fn wrapping_add(c: &char, add: u32) -> char {
    const MIN: u32 = 97;
    const MAX: u32 = 122;
    let int = *c as u32;
    let mut add = add;
    if add > 26 {
        add %= 26;
    }
    if int + add > MAX {
        let diff = MAX - int;
        add -= diff;
        return char::from_u32(96 + add).unwrap();
    }
    char::from_u32(int + add).unwrap()
}

fn count_letters(text: &str) -> Vec<(char, u32)> {
    let mut letters: HashMap<char, u32> = HashMap::new();

    for letter in text.chars() {
        match letter {
            '-' => {}
            _ => {
                letters.entry(letter).and_modify(|l| *l += 1).or_insert(1);
            }
        }
    }

    let mut list = letters
        .iter()
        .map(|(&l, &c)| (l, c))
        .collect::<Vec<(char, u32)>>();

    list.sort_by(
        |(l, c), (l2, c2)| {
            if c == c2 {
                l.cmp(l2)
            } else {
                c2.cmp(c)
            }
        },
    );

    list
}

fn verify_checksum(room: &str) -> bool {
    let (name, _, checksum) = discombobulate(room);

    let letter_count = count_letters(&name);
    if letter_count.len() >= 5 {
        let test = letter_count
            .iter()
            .take(5)
            .map(|(l, _)| l.to_string())
            .collect::<Vec<String>>()
            .join("");

        // println!("{}", test);

        return checksum == test;
    }

    false
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_wrapping_add() {
        assert_eq!(wrapping_add(&'a', 1), 'b');

        assert_eq!(wrapping_add(&'z', 1), 'a');

        assert_eq!(wrapping_add(&'q', 343), 'v');
        assert_eq!(wrapping_add(&'z', 343), 'e');
    }

    #[test]
    fn test_decrypt_name() {
        let name = "qzmt-zixmtkozy-ivhz";
        let id = 343;

        assert_eq!(decrypt_name(name, id), "very encrypted name");
    }
}
