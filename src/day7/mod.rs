use crate::read_lines;
use fancy_regex::Regex;
use lazy_static::lazy_static;

pub fn part1() {
    let lines: Vec<String> = read_lines("./inputs/day7")
        .map(|line| line.unwrap())
        .collect();

    adv_main(lines);
}

fn supports_tls(ip: &str) -> bool {
    lazy_static! {
        static ref ABBA: Regex = Regex::new(r"(.)(.)\2\1").unwrap();
        static ref HYPERNET: Regex = Regex::new(r"\[(?:.+)?((.)(.)\3\2)(?:.+)?\]").unwrap();
        static ref BAD_ABBA: Regex = Regex::new(r"(.)\1\1\1").unwrap();
    }

    if !HYPERNET.is_match(ip).unwrap() && ABBA.is_match(ip).unwrap() {
        let caps = ABBA.captures(ip).unwrap().unwrap();
        let abba = caps.get(0).unwrap().as_str();

        return !BAD_ABBA.is_match(abba).unwrap();
    }

    false
}

fn is_abba(slice: &str) -> bool {
    let mut in_hypernet = false;
    let mut valid = false;
    let slice: Vec<_> = slice.chars().collect();

    for window in slice.windows(4) {
        if window[0] == '[' || window[0] == ']' {
            in_hypernet = !in_hypernet;
            continue;
        }

        if window[0] != window[1] && window[1] == window[2] && window[0] == window[3] {
            if in_hypernet {
                return false;
            } else {
                valid = true;
            }
        }
    }

    valid
}

fn is_aba(slice: &str) -> bool {
    let (mut abas, mut babs) = (Vec::new(), Vec::new());
    let mut in_hypernet = false;
    let slice: Vec<_> = slice.chars().collect();

    for window in slice.windows(3) {
        if window[0] == '[' || window[0] == ']' {
            in_hypernet = !in_hypernet;
            continue;
        }

        if window[0] == window[2] && window[0] != window[1] {
            if in_hypernet {
                babs.push((window[1], window[0], window[1]));
            } else {
                abas.push((window[0], window[1], window[0]));
            }
        }
    }

    for aba in &abas {
        for bab in &babs {
            if aba == bab {
                return true;
            }
        }
    }

    false
}

fn adv_main(input: Vec<String>) {
    let (tls, ssl) = input
        .iter()
        .map(|s| (is_abba(s) as u64, is_aba(s) as u64))
        .fold((0, 0), |(t, s), (tls, ssl)| (t + tls, s + ssl));

    println!("tls support: {}\nssl support: {}", tls, ssl);
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_support_tls() {
        assert!(supports_tls("abba[mnop]qrst"));
        assert!(!supports_tls("abcd[bddb]xyyx"));
        assert!(!supports_tls("aaaa[qwer]tyui"));
        assert!(supports_tls("ioxxoj[asdfgh]zxcvbn"));

        assert!(!supports_tls("zeebynirxqrjbdqzjav[cawghcfvfeefkmx]xqcdkvawumyayfnq[qhhwzlwjvjpvyavtm]sbnvwssglfpyacfbua[wpbknuubmsjjbekkfy]icimffaoqghdpvsbx"));
        assert!(supports_tls(
            "vjqhodfzrrqjshbhx[lezezbbswydnjnz]ejcflwytgzvyigz[hjdilpgdyzfkloa]mxtkrysovvotkuyekba"
        ));
    }
}
