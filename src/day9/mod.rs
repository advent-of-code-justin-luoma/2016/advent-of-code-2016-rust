use lazy_static::lazy_static;
use regex::Regex;

use crate::read_file;

pub fn part1() {
    let text = read_file("./inputs/day9");
    
    let decompressed = decompress(&text);

    println!("{decompressed}");
    println!("{}", decompressed.trim().len());
}

pub fn part2() {
    let text = read_file("./inputs/day9");
    
    let mut decompressed = decompress(&text);

    loop {
        if decompressed.contains("(") {
            decompressed = decompress(&decompressed);
        } else {
            break;
        }
    }

    println!("{}", decompressed.trim().len());
}

pub fn example() {
    // println!("{}", decompress("X(8x2)(3x3)ABCY"));
    // println!("{}", decompress("X(3x3)ABC(3x3)ABCY"));

    let mut decompressed = decompress("(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN");
    loop {
        if decompressed.contains("(") {
            decompressed = decompress(&decompressed);
        } else {
            break;
        }
    }

    println!("{}", decompressed.len());
    // decompress("ADVENT");
    // decompress("A(1x5)BC");
    // decompress("(3x3)XYZ");
    // decompress("(6x1)(1x3)A");
    // decompress("X(8x2)(3x3)ABCY");
}

fn decompress(text: &str) -> String {
    let mut buf = String::new();

    let mut iter = text.chars().peekable();

    let mut operator = String::new();

    loop {
        if let Some(&'(') = iter.peek() {
            if operator.is_empty() {
                loop {
                    if let Some(&')') = iter.peek() {
                        operator.push(iter.next().unwrap());
                        break;
                    } else {
                        operator.push(iter.next().unwrap());
                    }
                }
            }
        }

        if !operator.is_empty() {
            // println!("{operator}");
            let caps = parse_operator(&operator);
            let decompressed = process_operator(&mut iter, &caps);
            buf.push_str(&decompressed);
            operator.clear();
        } else {
            buf.push(iter.next().unwrap());
        }

        if let None = iter.peek() {
            break;
        }
    }

    buf
}

fn process_operator(
    iter: &mut std::iter::Peekable<std::str::Chars>,
    caps: &regex::Captures,
) -> String {
    let chars: usize = caps.name("chars").unwrap().as_str().parse().unwrap();
    let repeat: usize = caps.name("repeat").unwrap().as_str().parse().unwrap();

    let mut buf = String::new();
    for l in iter.take(chars) {
        buf.push(l);
    }
    buf.repeat(repeat)
}

fn parse_operator(operator: &str) -> regex::Captures {
    lazy_static! {
        static ref OPERATOR: Regex = Regex::new(r"\((?P<chars>\d+)x(?P<repeat>\d+)\)").unwrap();
    }

    OPERATOR.captures(operator).unwrap()
}
