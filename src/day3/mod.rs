use crate::read_file;

pub fn day3_part1() {
    let parsed = parse(read_file("./inputs/day3"));

    let mut count = 0;

    for line in parsed.iter() {
        if let &[_, _, _] = line.as_slice() {
            if check_valid_triangle(line) {
                count += 1;
            }
        }
    }

    println!("{:?}", count);
}

pub fn part2() {
    let parsed = parse(read_file("./inputs/day3"));

    let mut count = 0;

    let mut triangle1 = vec![];
    let mut triangle2 = vec![];
    let mut triangle3 = vec![];

    for line in parsed.iter() {
        if let &[t1, t2, t3] = line.as_slice() {
            triangle1.push(t1);
            triangle2.push(t2);
            triangle3.push(t3);
            if triangle1.len() == 3 {
                if check_valid_triangle(&triangle1) {
                    count += 1;
                }
                triangle1.clear();
            }
            if triangle2.len() == 3 {
                if check_valid_triangle(&triangle2) {
                    count += 1;
                }
                triangle2.clear();
            }
            if triangle3.len() == 3 {
                if check_valid_triangle(&triangle3) {
                    count += 1;
                }
                triangle3.clear();
            }
        }
    }

    if !triangle1.is_empty() {
        println!("{:?}", triangle1);
    }

    // triangle2.chunks_exact(3)

    println!("{}", count);
}

fn check_valid_triangle(tri: &Vec<u32>) -> bool {
    if let &[a, b, c] = tri.as_slice() {
        return a + b > c && a + c > b && b + c > a;
    }

    false
}

fn parse(str: String) -> Vec<Vec<u32>> {
    str.trim()
        .split('\n')
        .map(|s| s.split_whitespace().map(|s| s.parse().unwrap()).collect())
        .collect()
}
