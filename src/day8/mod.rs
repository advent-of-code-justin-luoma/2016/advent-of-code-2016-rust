use core::fmt;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;

use crate::read_lines;

#[derive(Debug)]
struct Grid {
    rows: Vec<HashMap<usize, bool>>,
    dims: (usize, usize),
}

#[derive(Debug)]
enum Axis {
    X(usize),
    Y(usize),
}

impl Grid {
    fn new(rows: usize, cols: usize) -> Grid {
        let mut grid_rows = Vec::with_capacity(rows);
        for _ in 0..rows {
            let mut map = HashMap::new();

            for i in 0..cols {
                map.insert(i, false);
            }

            grid_rows.push(map);
        }

        Self {
            rows: grid_rows,
            dims: (rows, cols),
        }
    }

    fn rect(&mut self, width: usize, height: usize) {
        for row in 0..height {
            let row = self.rows.get_mut(row).unwrap();
            for col in 0..width {
                *row.get_mut(&col).unwrap() = true;
            }
        }
    }

    fn rotate(&mut self, axis: Axis, amount: usize) {
        match axis {
            Axis::X(col) => {
                for _ in 0..amount {
                    let last = self
                        .rows
                        .get(self.dims.0 - 1)
                        .unwrap()
                        .get(&col)
                        .unwrap()
                        .to_owned();
                    let mut prev = self.rows.get(0).unwrap().get(&col).unwrap().to_owned();
                    for row_i in 0..self.dims.0 {
                        let row = self.rows.get_mut(row_i).unwrap();
                        if row_i == 0 {
                            row.entry(col).and_modify(|e| *e = last);
                        } else {
                            let tmp = prev;
                            row.entry(col).and_modify(|e| {
                                prev = *e;
                                *e = tmp;
                            });
                        }
                    }
                }
            }
            Axis::Y(row) => {
                for _ in 0..amount {
                    // get the row for and_modify
                    let row = self.rows.get_mut(row).unwrap();
                    // get the last value in the row
                    let last = row.get(&(self.dims.1 - 1)).unwrap().to_owned();
                    // we need to keep track of the previous value to move it forward
                    let mut prev = row.get(&0).unwrap().to_owned();
                    for col in 0..self.dims.1 {
                        let tmp = prev;
                        if col == 0 {
                            row.entry(col).and_modify(|e| *e = last);
                        } else {
                            row.entry(col).and_modify(|e| {
                                prev = *e;
                                *e = tmp;
                            });
                        }
                    }
                }
            }
        }
    }
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut out = String::new();
        for row in self.rows.iter() {
            for col in 0..self.dims.1 {
                out.push_str(if *row.get(&col).unwrap() { "#" } else { "." })
            }
            out.push_str("\n");
        }
        write!(f, "{out}")
    }
}

pub fn example() {
    let mut grid = Grid::new(3, 7);

    grid.rect(3, 2);

    grid.rotate(Axis::X(1), 1);

    grid.rotate(Axis::Y(0), 4);

    grid.rotate(Axis::X(1), 1);

    grid.rect(3, 2);

    println!("{grid}");
}

#[derive(Debug)]
enum Ins {
    Rect(usize, usize),
    Rotate(Axis, usize),
}

pub fn part1() {
    let mut grid = Grid::new(6, 50);
    for line in read_lines("./inputs/day8") {
        let line = line.unwrap();

        match parse(&line) {
            Ins::Rect(width, height) => grid.rect(width, height),
            Ins::Rotate(axis, amount) => grid.rotate(axis, amount),
        }
    }

    println!("{grid}");
}

fn parse(text: &str) -> Ins {
    lazy_static! {
        static ref RECT: Regex = Regex::new(r"rect\s(\d+)x(\d+)").unwrap();
        static ref ROTATE: Regex = Regex::new(r"rotate\s(\w+)\s\w=(\d+)\sby\s(\d+)").unwrap();
    }

    return if let Some(caps) = RECT.captures(text) {
        Ins::Rect(
            caps.get(1).unwrap().as_str().parse().unwrap(),
            caps.get(2).unwrap().as_str().parse().unwrap(),
        )
    } else {
        let caps = ROTATE.captures(text).unwrap();
        return match caps.get(1).unwrap().as_str() {
            "column" => Ins::Rotate(
                Axis::X(caps.get(2).unwrap().as_str().parse().unwrap()),
                caps.get(3).unwrap().as_str().parse().unwrap(),
            ),
            "row" => Ins::Rotate(
                Axis::Y(caps.get(2).unwrap().as_str().parse().unwrap()),
                caps.get(3).unwrap().as_str().parse().unwrap(),
            ),
            _ => unreachable!(),
        };
    };
}
