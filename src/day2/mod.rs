use crate::read_file;

pub fn day2_part1() {
    let parsed = parse(read_file("./inputs/day2"));

    let mut button = 5;

    let mut code = vec![];

    for line in parsed.iter() {
        for ins in line.iter() {
            match ins.as_str() {
                "U" => {
                    if button - 3 > 0 {
                        button -= 3;
                    }
                }
                "R" => {
                    let tmp = button + 1;
                    if !(tmp > 9 || tmp == 4 || tmp == 7) {
                        button += 1;
                    }
                }
                "L" => {
                    let tmp = button - 1;
                    if !(tmp < 1 || tmp == 3 || tmp == 6) {
                        button -= 1;
                    }
                }
                "D" => {
                    if button + 3 < 10 {
                        button += 3;
                    }
                }
                _ => unreachable!(),
            }
        }
        code.push(button);
    }
    println!("{:?}", code);
}

pub fn day2_part2() {
    let keypad = vec![
        vec![None, None, Some("1"), None, None],
        vec![None, Some("2"), Some("3"), Some("4"), None],
        vec![Some("5"), Some("6"), Some("7"), Some("8"), Some("9")],
        vec![None, Some("A"), Some("B"), Some("C"), None],
        vec![None, None, Some("D"), None, None],
    ];

    let parsed = parse(read_file("./inputs/day2"));

    let mut code = vec![];

    let (mut x, mut y) = (2, 0);

    for line in parsed.iter() {
        for ins in line.iter() {
            match ins.as_str() {
                "U" => {
                    if x > 0 {
                        let button = keypad.get(x - 1).unwrap().get(y).unwrap();
                        if button.is_some() {
                            x -= 1;
                        }
                    }
                }
                "R" => {
                    if y < 4 {
                        let button = keypad.get(x).unwrap().get(y + 1).unwrap();
                        if button.is_some() {
                            y += 1;
                        }
                    }
                }
                "L" => {
                    if y > 0 {
                        let button = keypad.get(x).unwrap().get(y - 1).unwrap();
                        if button.is_some() {
                            y -= 1;
                        }
                    }
                }
                "D" => {
                    if x < 4 {
                        let button = keypad.get(x + 1).unwrap().get(y).unwrap();
                        if button.is_some() {
                            x += 1;
                        }
                    }
                }
                _ => unreachable!(),
            }
        }
        let button = keypad.get(x).unwrap().get(y).unwrap();
        code.push(button);
    }

    println!("{:?}", code);
}

fn parse(str: String) -> Vec<Vec<String>> {
    str.split_whitespace()
        .map(|s| {
            s.split("")
                .filter_map(|s| match s {
                    "" => None,
                    _ => Some(s.to_owned()),
                })
                .collect()
        })
        .collect()
}
